# nRF SDK 17.0.0
This is only **mirror** of official Nordic SDK. I'm using it for my project with Nordic's products.  
You can get original files, documentation and license on [official website](https://www.nordicsemi.com/Software-and-tools/Software/nRF5-SDK)
